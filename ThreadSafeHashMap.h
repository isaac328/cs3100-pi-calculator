#pragma once
#include <unordered_map>
#include <iostream>
#include <mutex>

class ThreadSafeHashMap
{
private:
	std::unordered_map<int, int>* map;
	std::mutex mtx;
	int size;

public:
	ThreadSafeHashMap(int);
	~ThreadSafeHashMap();
	void insert(int, int);
	void printDigits();
};
