#include "ThreadSafeHashMap.h"

ThreadSafeHashMap::ThreadSafeHashMap(int size)
{
	map = new std::unordered_map<int, int>();
	map->reserve(size);
	ThreadSafeHashMap::size = size;
}

ThreadSafeHashMap::~ThreadSafeHashMap()
{
	delete map;
}

void ThreadSafeHashMap::insert(int position, int calculatedDigit)
{
	mtx.lock();
	std::pair<int, int> piDigit(position, calculatedDigit);
	map->insert(piDigit);
	mtx.unlock();
}

void ThreadSafeHashMap::printDigits()
{
	for (int i = 1; i <= size; i++)
	{
		std::unordered_map<int, int>::iterator search = map->find(i);

		if (search != map->end())
			std::cout << search->second;
		else
			std::cout << std::endl << "Not Found" << std::endl;
	}
	std::cout << std::endl;
}