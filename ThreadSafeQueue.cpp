#include "ThreadSafeQueue.h"

ThreadSafeQueue::ThreadSafeQueue(int piDigits)
{
	for (int i = 1; i <= piDigits; i++)
		queue.push(i);
	mtx = new std::mutex();
}

ThreadSafeQueue::~ThreadSafeQueue()
{
	delete mtx;
}

int ThreadSafeQueue::remove()
{
	mtx->lock();
	if (!queue.empty())
	{
		int digit = queue.front();
		queue.pop();
		mtx->unlock();
		return digit;
	}
	mtx->unlock();
}
