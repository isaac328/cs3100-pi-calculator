#pragma once
#include<iostream>
#include<queue>
#include<mutex>

class ThreadSafeQueue
{
private:
	std::queue<int> queue;
	std::mutex* mtx;

public:
	ThreadSafeQueue(int);
	~ThreadSafeQueue();
	int remove();
};
